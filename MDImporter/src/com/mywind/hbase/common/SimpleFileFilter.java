/**
 * 
 */
package com.mywind.hbase.common;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * @author a01513
 *
 */
public class SimpleFileFilter extends FileFilter {
	
	@Override
	public boolean accept(File f) {
		if(f != null){
			if(f.isDirectory())
				return true;
		}
		return f.getName().endsWith(".txt");
	}

	@Override
	public String getDescription() {
		return ".txt";
	}

}
