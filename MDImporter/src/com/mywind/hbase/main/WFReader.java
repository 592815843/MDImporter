package com.mywind.hbase.main;

import java.io.File;
import java.util.Iterator;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;


public class WFReader{
	
	public static void main(String[] args) throws Exception{
		String tableName = "windfarm";
		//String[] family = {"prop"}, qualifier = {"code","name"};
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "192.168.8.184,192.168.8.183,192.168.8.178,192.168.8.190,192.168.8.167,192.168.8.169,192.168.8.170,192.168.8.172,192.168.8.182,192.168.8.175,192.168.8.179");   
        conf.set("hbase.zookeeper.property.clientPort", "2181");   
        try {
        	File workaround = new File(".");
            System.getProperties().put("hadoop.home.dir", workaround.getAbsolutePath());
            new File("./bin").mkdirs();
            new File("./bin/winutils.exe").createNewFile();
			HBaseAdmin admin = new HBaseAdmin(conf);
			if(admin.tableExists(tableName)){
				HTable table = new HTable(conf, tableName);
				Scan scan = new Scan();
				ResultScanner rsc = table.getScanner(scan);
				Iterator<Result> it = rsc.iterator();
				while(it.hasNext()){
					for(Cell cell:it.next().rawCells()){
						System.out.println("RowKey=>"+new String(cell.getRowArray(),cell.getRowOffset(),cell.getRowLength(),"UTF-8")+"->"+new String(cell.getFamilyArray(),cell.getFamilyOffset(),cell.getFamilyLength(),"UTF-8")+":"+new String(cell.getQualifierArray(),cell.getQualifierOffset(),cell.getQualifierLength(),"UTF-8")+"="+new String(cell.getValueArray(),cell.getValueOffset(),cell.getValueLength(),"UTF-8"));
					}
				}
				table.close();
			}
			admin.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	
}
